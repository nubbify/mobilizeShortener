## How to build and run
1. Install Go 1.16
2. Clone the repository and move to the root directory    
3. Run ```go build``` in the root directory from any command line with access to the PATH that has Go.exe
4. Run the built executable (```sudo .\mobilizeShortener``` or ```.\mobilizeShortener.exe``` depending on your OS)

## Tech Stack
Language: Go. Quick for prototyping. Easily builds into executables that run on any machine. I also work with it (big plus).

Database: In-memory SQLite. Also quick for prototyping. Doesn't require an entire server set up. 
Doesn't quite scale incredibly well but this shouldn't be seeing too much traffic and Go's SQL
wrappers make it easy to switch between drivers. 

## Design and Architecture
Database design was probably the first thing I sat down and did. I just thought to myself how I wanted to represent 
the links and the stats associated with them. In the end I settled with two tables; one to store the information relating to the links and another to store information relating to how users engaged with them.

The idea was that stats could just be queries on the visits table (count unique visits to this link_id, group by day, etc.). Leveraging SQL's built in statistical aggregators would be much easier than trying to do it all serverside myself. 

The database holds just a randomized set of 7 characters. Were this to get much larger, I imagine that those values would need to get bigger or there would need to be some sort of timeout on them. Were the data to get large enough, it might be efficient to hash the url and then save that as a long and use that as an ID (numbers index much better than strings). We could deal with collisions by adding just adding 0x01 to the hex until we found a free space and then it would hopefully not take too long to search.  

As for the code itself, the code is broken down into 4 folders:

1. /data/ handles interacting with the database. Queries, setting it up, etc.
2. /handlers/ deal with organizing the different pieces of the routing table. We group all of the link based endpoints together into a file, the stats into another and point the router to these handlers.
3. /middleware/ logs the requests to the server. I was debating creating a middleware piece for visiting sites, but felt that because there's only really one redirection endpoint, I could just put tracking logic in there. Maybe if I were to extend this, I might want to build some middleware to just see what the most popular endpoints are.
4. /models/ stores the models we use to marshall json and read from the database. 

I decided to break it down this way to keep a separation of concerns between different layers of the application. Routing logic shouldn't have any handler code with it. Handlers should only focus on parsing incoming data from both users and the database and not need to deal with queries, etc.

Apparently this makes it a lot easier to develop tests. Wild stuff. 

## API endpoints
1. POST endpoint route: `/create`

Accepts the following JSON Body:
```javascript
{
   "destination": string required, //(must start with http)
   "shortenedName": string optional
}  
```
Creates a shortened link (customized if you choose) that can be redirected to the destination.
Returns 400 if the link is not an http link, 409 if the shortened link already exists, 500 if there is an internal database error.
Returns the combination of destination and shortenedName if successful.

2. GET endpoint route: `/stats/{link}`

Returns a json with stats relating to the shortened link specified.

3. GET endpoint route: `/stats`
   
Returns a json with site-wide stats

4. GET endpoint route: `/{link}`

Redirects to the destination stored for the link or returns a 404 if the link doesn't exist.
