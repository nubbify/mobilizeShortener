package data

import (
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	_ "modernc.org/sqlite"
	"os"
)

var schema = `
CREATE TABLE IF NOT EXISTS links (
	link_id    	INTEGER 		PRIMARY KEY,
	shortened	VARCHAR(80)  	DEFAULT '' UNIQUE,
    destination VARCHAR(250)  	DEFAULT '',
	created     TIMESTAMP	 	DEFAULT CURRENT_TIMESTAMP,
	custom		BOOLEAN 	 	DEFAULT 0
);

CREATE TABLE IF NOT EXISTS visits (
    link_id		INTEGER,
	visit_time	TIMESTAMP		DEFAULT CURRENT_TIMESTAMP,
	visitor		VARCHAR(45),
    FOREIGN KEY (link_id) REFERENCES links (link_id)
);
`

func InitDatabase() {
	db, err := sqlx.Connect("sqlite", "./data.db")
	if err != nil {
		logrus.Fatalln(err)
		panic(err)
	}

	db.MustExec(schema)
}

func ResetDatabase() {
	db, err := sqlx.Connect("sqlite", "./data.db")
	if err != nil {
		logrus.Fatalln(err)
		panic(err)
	}

	db.MustExec("DROP TABLE IF EXISTS links; DROP TABLE IF EXISTS visits;")
	db.MustExec(schema)
}

func DeleteDatabase() {
	os.Remove("./data.db")
}

func GetDatabase() (db *sqlx.DB, err error) {
	db, err = sqlx.Connect("sqlite", "./data.db")
	if err != nil {
		logrus.Fatalln(err)
	}
	return
}
