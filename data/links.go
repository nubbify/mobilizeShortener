package data

import (
	"database/sql"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"math/rand"
	"mobilizeShortener/models"
)

func GetExistingLinkDataForShortenedLink(db *sqlx.DB, shortenedLink string) (data models.Link, err error) {
	err = db.QueryRowx("SELECT * FROM links WHERE shortened = ?",
		shortenedLink).StructScan(&data)
	return
}

func CreateShortenedLink(db *sqlx.DB, destination, customShortened string) (result sql.Result, err error) {
	var shortened string
	if destination[0:4] != "http" {
		err = errors.New("destination is not http link")
		return
	}
	if customShortened != "" {
		result, err = db.Exec(
			"INSERT INTO links (shortened, destination, custom) VALUES (?, ?, 1)",
			customShortened,
			destination,
		)
	} else {
		for {
			shortened = randStringBytes(7)
			exists, _ := CheckIfShortenedLinkExists(db, shortened)
			if !exists {
				break
			}
		}

		result, err = db.Exec(
			"INSERT INTO links (shortened, destination) VALUES (?, ?)",
			shortened,
			destination,
		)
	}
	return
}

func GetShortenedLinkFromId(db *sqlx.DB, result int64) (shortened string, err error) {
	err = db.QueryRowx(
		"SELECT shortened FROM links WHERE link_id=?",
		result,
	).Scan(&shortened)
	return
}

func GetIdFromShortenedLink(db *sqlx.DB, shortened string) (result int64, err error) {
	err = db.QueryRowx(
		"SELECT link_id FROM links WHERE shortened=?",
		shortened,
	).Scan(&result)
	return
}

func GetExistingLinkForDestination(db *sqlx.DB, destination string) (existingShortenedLink string, err error) {
	if rowExists(db, "SELECT shortened FROM links WHERE destination = ? AND custom = 0",
		destination) {
		err = db.QueryRowx("SELECT shortened FROM links WHERE destination = ? AND custom = 0",
			destination).Scan(&existingShortenedLink)
	}
	return
}

func CheckIfCustomLinkExists(db *sqlx.DB, customLink string) (exists bool, err error) {
	if rowExists(db, "SELECT shortened FROM links WHERE shortened = ? AND custom = 1", customLink) {
		exists = true
	}
	return
}

func CheckIfShortenedLinkExists(db *sqlx.DB, shortenedLink string) (exists bool, err error) {
	if rowExists(db, "SELECT shortened FROM links WHERE shortened = ?", shortenedLink) {
		exists = true
	}
	return
}

func rowExists(db *sqlx.DB, query string, args ...interface{}) (exists bool) {
	query = fmt.Sprintf("SELECT exists (%s)", query)
	err := db.QueryRowx(query, args...).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		logrus.Error("Error checking if row exists '%s'; %v", args, err)
	}
	return
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

func randStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}
