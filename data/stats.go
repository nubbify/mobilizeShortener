package data

import (
	"github.com/jmoiron/sqlx"
	"mobilizeShortener/models"
)

func GetUniqueVisitorsOfLinkId(db *sqlx.DB, linkId int) (visitors int, err error) {
	err = db.QueryRowx(
		"SELECT COUNT(DISTINCT(visitor)) FROM visits WHERE link_id=?",
		linkId,
	).Scan(&visitors)
	return
}

func GetTotalVisitorsOfLinkId(db *sqlx.DB, linkId int) (visitors int, err error) {
	err = db.QueryRowx(
		"SELECT COUNT(*) FROM visits WHERE link_id=?",
		linkId,
	).Scan(&visitors)
	return
}

func GetDailyVisitorsOfLinkId(db *sqlx.DB, linkId int) (visitors []models.DailyStats, err error) {
	rows, err := db.Queryx(
		"SELECT strftime('%Y %m %d', visit_time) AS date, count(*) AS count "+
			"FROM visits "+
			"WHERE link_id = ? "+
			"GROUP BY strftime('%Y %m %d', visit_time)",
		linkId,
	)

	for rows.Next() {
		var day = models.DailyStats{}
		rows.StructScan(&day)
		visitors = append(visitors, day)
	}
	return
}

func GetUniqueVisitorsOfSite(db *sqlx.DB) (visitors int, err error) {
	err = db.QueryRowx(
		"SELECT COUNT(DISTINCT(visitor)) FROM visits",
	).Scan(&visitors)
	return
}

func GetTotalVisitorsOfSite(db *sqlx.DB) (visitors int, err error) {
	err = db.QueryRowx(
		"SELECT COUNT(*) FROM visits",
	).Scan(&visitors)
	return
}

func GetDailyVisitorsOfSite(db *sqlx.DB) (visitors []models.DailyStats, err error) {
	rows, err := db.Queryx(
		"SELECT strftime('%Y %m %d', visit_time) AS date, count(*) AS count " +
			"FROM visits " +
			"GROUP BY strftime('%Y %m %d', visit_time)",
	)

	for rows.Next() {
		var day = models.DailyStats{}
		rows.StructScan(&day)
		visitors = append(visitors, day)
	}
	return
}

func GetVisitorsPerSite(db *sqlx.DB) (visitors []models.GlobalLinkStats, err error) {
	//TODO: coalesce each link's date-counts into one array to avoid repetition of link in output.
	rows, err := db.Queryx(
		"SELECT destination, strftime('%Y %m %d', visit_time) AS date, count(*) AS count " +
			"FROM visits JOIN links ON links.link_id = visits.link_id " +
			"GROUP BY  strftime('%Y %m %d', visit_time)",
	)

	for rows.Next() {
		var row = models.GlobalLinkStats{}
		rows.StructScan(&row)
		visitors = append(visitors, row)
	}
	return
}
