package data

import (
	"github.com/jmoiron/sqlx"
)

func RecordVisit(db *sqlx.DB, linkID int, visitor string) (err error) {
	_, err = db.Exec(
		"INSERT INTO visits (link_id, visitor) VALUES (?, ?)",
		linkID,
		visitor,
	)
	return
}
