module mobilizeShortener

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/sirupsen/logrus v1.8.1
	modernc.org/sqlite v1.10.8 // indirect
)
