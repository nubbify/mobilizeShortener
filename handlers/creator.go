package handlers

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"mobilizeShortener/data"
	models "mobilizeShortener/models"
	"net/http"
)

// LinkCreationHandler creates a random short link for an arbitrary URL
// Accepts a models.LinkData
// Returns a models.LinkData and 200 on success
// Returns a 409 if the shortened link is already taken
// Returns a 500 if the server or database has problems
func LinkCreationHandler(w http.ResponseWriter, r *http.Request) {
	db, err := data.GetDatabase()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer db.Close()

	var newLink models.LinkData
	err = json.NewDecoder(r.Body).Decode(&newLink)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if newLink.Destination[0:4] != "http" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var returnValue models.LinkData
	if exists, _ := data.CheckIfCustomLinkExists(db, newLink.ShortenedName); exists {
		w.WriteHeader(http.StatusConflict)
		return
	} else if existingLink, _ := data.GetExistingLinkForDestination(db, newLink.Destination); existingLink != "" {
		returnValue = models.LinkData{
			ShortenedName: existingLink,
			Destination:   newLink.Destination,
		}
	} else {
		result, err := data.CreateShortenedLink(db, newLink.Destination, newLink.ShortenedName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		resultId, _ := result.LastInsertId()
		shortenedLink, err := data.GetShortenedLinkFromId(db, resultId)

		returnValue = models.LinkData{
			ShortenedName: shortenedLink,
			Destination:   newLink.Destination,
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	returnBytes, _ := json.Marshal(returnValue)
	if _, err := w.Write(returnBytes); err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
