package handlers

import (
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"mobilizeShortener/data"
	"mobilizeShortener/models"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func setUp() {
	data.InitDatabase()
}

func tearDown() {
	data.DeleteDatabase()
}

func TestMain(m *testing.M) {
	setUp()
	retCode := m.Run()
	tearDown()
	os.Exit(retCode)
}

func TestLinkCreationHandlerCustomOk(t *testing.T) {
	bodyModel := models.LinkData{
		ShortenedName: "test",
		Destination:   "http://localhost",
	}
	body, _ := json.Marshal(bodyModel)

	request, err := http.NewRequest("POST", "/create", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	ResponseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(LinkCreationHandler)

	handler.ServeHTTP(ResponseRecorder, request)

	if status := ResponseRecorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: Got %v but wanted %v",
			status, http.StatusOK)
	}

	if ResponseRecorder.Body.String() != string(body) {
		t.Errorf("Handler returned unexpected body: got %v but wanted %v",
			ResponseRecorder.Body.String(), string(body))
	}
	data.ResetDatabase()
}

func TestLinkCreationHandlerExistingLink(t *testing.T) {
	bodyModel := models.LinkData{
		Destination: "http://localhost",
	}
	body, _ := json.Marshal(bodyModel)

	request, err := http.NewRequest("POST", "/create", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	ResponseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(LinkCreationHandler)
	handler.ServeHTTP(ResponseRecorder, request)

	if status := ResponseRecorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: Got %v but wanted %v",
			status, http.StatusOK)
	}

	firstBody := ResponseRecorder.Body.String()

	request, err = http.NewRequest("POST", "/create", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	ResponseRecorder = httptest.NewRecorder()
	handler = LinkCreationHandler
	handler.ServeHTTP(ResponseRecorder, request)

	if status := ResponseRecorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: Got %v but wanted %v",
			status, http.StatusOK)
	}

	if ResponseRecorder.Body.String() != firstBody {
		t.Errorf("Handler returned unexpected body: got %v but wanted %v",
			ResponseRecorder.Body.String(), string(body))
	}
	data.ResetDatabase()
}

func TestLinkCreationHandlerExistingCustomLink(t *testing.T) {
	bodyModel := models.LinkData{
		ShortenedName: "test",
		Destination:   "http://localhost",
	}
	body, _ := json.Marshal(bodyModel)

	request, err := http.NewRequest("POST", "/create", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	ResponseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(LinkCreationHandler)
	handler.ServeHTTP(ResponseRecorder, request)

	if status := ResponseRecorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: Got %v but wanted %v",
			status, http.StatusOK)
	}

	if ResponseRecorder.Body.String() != string(body) {
		t.Errorf("Handler returned unexpected body: got %v but wanted %v",
			ResponseRecorder.Body.String(), string(body))
	}

	request, err = http.NewRequest("POST", "/create", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	ResponseRecorder = httptest.NewRecorder()
	handler = LinkCreationHandler
	handler.ServeHTTP(ResponseRecorder, request)

	if status := ResponseRecorder.Code; status != http.StatusConflict {
		t.Errorf("Handler returned wrong status code: Got %v but wanted %v",
			status, http.StatusConflict)
	}
	data.ResetDatabase()
}

func TestLinkRedirect(t *testing.T) {
	db, _ := data.GetDatabase()
	data.CreateShortenedLink(db, "http://localhost", "test")

	request, err := http.NewRequest("GET", "/test", nil)
	if err != nil {
		t.Fatal(err)
	}
	request = mux.SetURLVars(request, map[string]string{
		"url": "test",
	})

	ResponseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(RedirectionHandler)

	handler.ServeHTTP(ResponseRecorder, request)

	if status := ResponseRecorder.Code; status != http.StatusFound {
		t.Errorf("Handler returned wrong status code: Got %v but wanted %v",
			status, http.StatusFound)
	}

	data.ResetDatabase()
}
