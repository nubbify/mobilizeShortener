package handlers

import (
	"github.com/gorilla/mux"
	"mobilizeShortener/data"
	"net"
	"net/http"
)

// RedirectionHandler takes a link and redirects the incoming user to the extended link.
// Returns a 404 if the shortened link doesn't exist
// Returns a 500 if the server or database has problems
// Returns a 302 and redirects if the link exists
func RedirectionHandler(w http.ResponseWriter, r *http.Request) {
	db, err := data.GetDatabase()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer db.Close()

	params := mux.Vars(r)
	link, err := data.GetExistingLinkDataForShortenedLink(db, params["url"])

	if err != nil || link.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	host, _, _ := net.SplitHostPort(r.RemoteAddr)
	data.RecordVisit(db, link.Id, host)
	http.Redirect(w, r, link.Destination, http.StatusFound)
}
