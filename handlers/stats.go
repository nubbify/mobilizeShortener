package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"mobilizeShortener/data"
	"mobilizeShortener/models"
	"net/http"
)

// LinkStatHandler provides statistics on the usage of a specific link
// Returns a 404 if the shortened link doesn't exist
// Returns a 500 if the server or database has problems
func LinkStatHandler(w http.ResponseWriter, r *http.Request) {
	db, err := data.GetDatabase()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer db.Close()

	params := mux.Vars(r)
	link, _ := data.GetExistingLinkDataForShortenedLink(db, params["url"])
	if link.Id == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	uniqueVisitors, _ := data.GetUniqueVisitorsOfLinkId(db, link.Id)
	totalVisitors, _ := data.GetTotalVisitorsOfLinkId(db, link.Id)
	dailyVisitors, _ := data.GetDailyVisitorsOfLinkId(db, link.Id)

	results := models.LinkStats{
		Shortened: params["url"],
		Unique:    uniqueVisitors,
		Total:     totalVisitors,
		Daily:     dailyVisitors,
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	returnBytes, _ := json.Marshal(results)
	if _, err := w.Write(returnBytes); err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

// SiteStatHandler provides site-wide statistics
// Returns a 500 if the server or database has problems
func SiteStatHandler(w http.ResponseWriter, r *http.Request) {
	db, err := data.GetDatabase()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer db.Close()

	uniqueVisitors, _ := data.GetUniqueVisitorsOfSite(db)
	totalVisitors, _ := data.GetTotalVisitorsOfSite(db)
	dailyVisitors, _ := data.GetDailyVisitorsOfSite(db)
	siteLinkStats, _ := data.GetVisitorsPerSite(db)

	results := models.SiteStats{
		Unique: uniqueVisitors,
		Total:  totalVisitors,
		Daily:  dailyVisitors,
		Links:  siteLinkStats,
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	returnBytes, _ := json.Marshal(results)
	if _, err := w.Write(returnBytes); err != nil {
		logrus.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
