package middleware

import (
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		uri := r.RequestURI
		method := r.Method
		next.ServeHTTP(w, r)

		duration := time.Since(start)

		logrus.WithFields(logrus.Fields {
			"uri": 		uri,
			"method": 	method,
			"duration": duration,
		}).Info()
	})
}