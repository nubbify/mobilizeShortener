package models

import "time"

type Link struct {
	Id          int       `json:"link_id" db:"link_id"`
	Shortened   string    `json:"shortened" db:"shortened"`
	Destination string    `json:"destination" db:"destination"`
	Created     time.Time `json:"created" db:"created"`
	Custom      bool      `json:"custom" db:"custom"`
}

type Visit struct {
	Id      int       `json:"link_id" db:"link_id"`
	Time    time.Time `json:"time" db:"visit_time"`
	Visitor string    `json:"visitor" db:"visitor"`
}

type LinkData struct {
	ShortenedName string `json:"shortenedName" db:"shortened"`
	Destination   string `json:"destination" db:"destination"`
}

type LinkStats struct {
	Shortened string       `json:"shortenedName,omitempty"`
	Unique    int          `json:"unique"`
	Total     int          `json:"total"`
	Daily     []DailyStats `json:"daily"`
}

type SiteStats struct {
	Unique int               `json:"unique"`
	Total  int               `json:"total"`
	Daily  []DailyStats      `json:"daily"`
	Links  []GlobalLinkStats `json:"links"`
}

type DailyStats struct {
	Date  string `json:"date" db:"date"`
	Count string `json:"count" db:"count"`
}

type GlobalLinkStats struct {
	Destination string `json:"destination" db:"destination"`
	Date        string `json:"date" db:"date"`
	Count       int    `json:"count" db:"count"`
}
