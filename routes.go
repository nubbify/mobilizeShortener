package main

import (
	"github.com/gorilla/mux"
	"mobilizeShortener/handlers"
	"mobilizeShortener/middleware"
)

func setupRouter() (router *mux.Router) {
	router = mux.NewRouter()

	router.HandleFunc("/create", handlers.LinkCreationHandler).Methods("POST")
	router.HandleFunc("/stats", handlers.SiteStatHandler).Methods("GET")
	router.HandleFunc("/stats/{url}", handlers.LinkStatHandler).Methods("GET")
	router.HandleFunc("/{url}", handlers.RedirectionHandler).Methods("GET")

	// Set up middleware
	router.Use(mux.CORSMethodMiddleware(router))
	router.Use(middleware.Logging)

	return
}
