package main

import (
	"mobilizeShortener/data"
	"net/http"
)

func main() {
	router := setupRouter()
	data.InitDatabase()
	http.ListenAndServe(":80", router)
}
